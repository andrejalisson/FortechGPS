<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Clientes;
use CodePhix\Asaas\Asaas;

class ClienteController extends Controller{
    public function lista(){
        $title = "Clientes";
        return view('clientes.lista')->with(compact('title'));
    }

    public function add(){
        $title = "Adicionar Cliente";
        return view('clientes.add')->with(compact('title'));
    }

    public function edt($id){
        $cliente = Clientes::where('id', $id)->get();
        $title = "Editar Cliente";
        return view('clientes.edt')->with(compact('title', 'cliente'));
    }

    public function addPost(Request $request){
        $cliente = new Clientes;
        $cliente->name = $request->nome;
        $cliente->cpfCnpj = preg_replace('/[-\@\/\.\(\)\;\"  "]+/', '', $request->documento);
        $cliente->email = $request->email;
        $cliente->phone = preg_replace('/[-\@\.\(\)\;\" "]+/', '', $request->telefone);
        $cliente->mobilePhone = preg_replace('/[-\@\.\(\)\;\" "]+/', '', $request->celular);
        $cliente->address = $request->logradouro;
        $cliente->addressNumber = $request->numero;
        $cliente->complement = $request->complemento;
        $cliente->province = $request->bairro;
        $cliente->postalCode = preg_replace('/[-\@\.\(\)\;\" "]+/', '', $request->cep);
        $cliente->city = $request->cidade;
        $cliente->state = $request->uf;
        $cliente->notificationDisabled = true;
        $cliente->municipalInscription = $request->im;
        $cliente->stateInscription = $request->ie;
        $cliente->observations = $request->obs;
        $cliente->dateCreated = date('Y-m-d');
        $cliente->deleted = false;
        $dadosCliente = array(
            'name' => $request->nome,
            'cpfCnpj' => preg_replace('/[-\@\/\.\(\)\;\"  "]+/', '', $request->documento),
            'email' => $request->email,
            'phone' => preg_replace('/[-\@\.\(\)\;\" "]+/', '', $request->telefone),
            'mobilePhone' => preg_replace('/[-\@\.\(\)\;\" "]+/', '', $request->celular),
            'address' => $request->logradouro,
            'addressNumber' => $request->numero,
            'complement' => $request->complemento,
            'province' => $request->bairro,
            'postalCode' => preg_replace('/[-\@\.\(\)\;\" "]+/', '', $request->cep),
            'city' => $request->cidade,
            'state' => $request->uf,
            'notificationDisabled' => true,
            'municipalInscription' => $request->im,
            'stateInscription' => $request->ie,
            'observations' => $request->obs,
            'dateCreated' => date('Y-m-d'),
            'deleted' => false
        );
        $asaas = new Asaas('47ea68bd5ce79b36ff44c8e362f03f82cb4df2f82942974dd42f766f16b3ac08', 'homologacao');
        $idCli = $asaas->Cliente()->create($dadosCliente);
        $cliente->externalReference = $idCli->id;
        $cliente->save();
        $request->session()->flash('sucesso', 'Cliente cadastrado com sucesso.');
        return redirect('/Clientes');
    }

    public function edtPost(Request $request, $id){
        $cliente  = Clientes::find($id);
        $cliente->name = $request->nome;
        $cliente->cpfCnpj = preg_replace('/[-\@\/\.\(\)\;\"  "]+/', '', $request->documento);
        $cliente->email = $request->email;
        $cliente->phone = preg_replace('/[-\@\.\(\)\;\" "]+/', '', $request->telefone);
        $cliente->mobilePhone = preg_replace('/[-\@\.\(\)\;\" "]+/', '', $request->celular);
        $cliente->address = $request->logradouro;
        $cliente->addressNumber = $request->numero;
        $cliente->complement = $request->complemento;
        $cliente->province = $request->bairro;
        $cliente->postalCode = preg_replace('/[-\@\.\(\)\;\" "]+/', '', $request->cep);
        $cliente->city = $request->cidade;
        $cliente->state = $request->uf;
        $cliente->municipalInscription = $request->im;
        $cliente->stateInscription = $request->ie;
        $cliente->observations = $request->obs;
        $dadosCliente = array(
            'name' => $request->nome,
            'cpfCnpj' => preg_replace('/[-\@\/\.\(\)\;\"  "]+/', '', $request->documento),
            'email' => $request->email,
            'phone' => preg_replace('/[-\@\.\(\)\;\" "]+/', '', $request->telefone),
            'mobilePhone' => preg_replace('/[-\@\.\(\)\;\" "]+/', '', $request->celular),
            'address' => $request->logradouro,
            'addressNumber' => $request->numero,
            'complement' => $request->complemento,
            'province' => $request->bairro,
            'postalCode' => preg_replace('/[-\@\.\(\)\;\" "]+/', '', $request->cep),
            'city' => $request->cidade,
            'state' => $request->uf,
            'notificationDisabled' => true,
            'municipalInscription' => $request->im,
            'stateInscription' => $request->ie,
            'observations' => $request->obs,
            'dateCreated' => date('Y-m-d'),
            'deleted' => false
        );
        $asaas = new Asaas('47ea68bd5ce79b36ff44c8e362f03f82cb4df2f82942974dd42f766f16b3ac08', 'homologacao');
        $asaas->Cliente()->update($cliente->externalReference, $dadosCliente);
        $cliente->save();
        $request->session()->flash('sucesso', 'Cliente Editado com sucesso.');
        return redirect('/Clientes');
    }

    public function todosClientes(Request $request){
        $columns = array(
            0 =>'id',
            1 =>'name',
            2 =>'cpfCnpj',
            3 =>'email',
            4 =>'mobilePhone',
        );

        $totalData = Clientes::count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if(empty($request->input('search.value'))){
            $clientes = Clientes::offset($start)->limit($limit)->orderBy($order,$dir)->get();
        }
        else{
            $search = $request->input('search.value');
            $clientes =  Clientes::where('name','LIKE',"%{$search}%")
                                    ->orwhere('cpfCnpj','LIKE',"%{$search}%")
                                    ->orwhere('email','LIKE',"%{$search}%")
                                    ->orwhere('mobilePhone','LIKE',"%{$search}%")
                                    ->offset($start)
                                    ->limit($limit)
                                    ->orderBy($order,$dir)
                                    ->get();
            $totalFiltered = Clientes::where('name','LIKE',"%{$search}%")
                                    ->orwhere('cpfCnpj','LIKE',"%{$search}%")
                                    ->orwhere('email','LIKE',"%{$search}%")
                                    ->orwhere('mobilePhone','LIKE',"%{$search}%")
                                    ->count();
        }
        $data = array();

        if(!empty($clientes)){
            foreach ($clientes as $cliente){
                $nestedData['id'] = "# ".$cliente->id;
                $nestedData['nome'] =$cliente->name;
                $nestedData['documento'] =$cliente->cpfCnpj;
                $nestedData['email'] =$cliente->email;
                $nestedData['telefone'] =$cliente->mobilePhone;
                if($cliente->deleted == 0){
                    $nestedData['status'] = "<span class=\"badge badge-light-success\">ATIVO</span>";
                }else{
                    $nestedData['status'] = "<span class=\"badge badge-light-danger\">DESATIVADO</span>";
                }
                $nestedData['opcoes'] = "<a href=\"/Cliente/".$cliente->id."\" class=\"btn btn-icon btn-success waves-effect waves-float waves-light\"><i data-feather='eye'></i></a>
                                            <a href=\"/EdtClientes/".$cliente->id."\" class=\"btn btn-icon btn-warning waves-effect waves-float waves-light\"><i data-feather='edit'></i></i></a>
                                            <a href=\"/Cliente/".$cliente->id."\" class=\"btn btn-icon btn-danger waves-effect waves-float waves-light\"><i data-feather='trash-2'></i></i></i></a>
                                            ";

                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);
    }

    public function cliente($id){
        $title = "Cliente";
        $cliente =  Clientes::where('id', $id)->get();
        return view('clientes.view')->with(compact('title', 'cliente'));
    }

    public function importAsaas(Request $request){
        $asaas = new Asaas('d6a36ccab823ca9680da02cd73fa61a5c1ec5dad42a55fadd4cd299c50e850b5', 'producao');
        $filtro = array(
            'limit' => 100,
        );
        $clientes = $asaas->Cliente()->getAll($filtro);
        foreach($clientes->data as $dados){
            $cliente = new Clientes;
            $cliente->name = $dados->name;
            $cliente->externalReference = $dados->id;
            $cliente->cpfCnpj = $dados->cpfCnpj;
            $cliente->email = $dados->email;
            $cliente->phone = $dados->phone;
            $cliente->mobilePhone = $dados->mobilePhone;
            $cliente->address = $dados->address;
            $cliente->addressNumber = $dados->addressNumber;
            $cliente->complement = $dados->complement;
            $cliente->province = $dados->province;
            $cliente->postalCode = $dados->postalCode;
            $cliente->state = $dados->state;
            $cliente->observations = $dados->observations;
            $cliente->save();
            echo $dados->name." importado com sucesso. <br>";
        }
        $request->session()->flash('sucesso', 'Clientes importados do Asaas.');
        return redirect()->back();
    }

    public function importAsaasHomolog(Request $request){
        $asaas = new Asaas('47ea68bd5ce79b36ff44c8e362f03f82cb4df2f82942974dd42f766f16b3ac08', 'homologacao');
        $clientes = Clientes::all();
        foreach($clientes as $clientes){
            $dadosCliente = array(
                'name' => $clientes->name,
                'cpfCnpj' => $clientes->cpfCnpj,
                'email' => $clientes->email,
                'phone' => $clientes->phone,
                'mobilePhone' => $clientes->mobilePhone,
                'address' => $clientes->address,
                'addressNumber' => $clientes->addressNumber,
                'complement' => $clientes->complement,
                'province' => $clientes->province,
                'postalCode' => $clientes->postalCode,
                'state' => $clientes->state,
                'notificationDisabled' => true,
                'observations' => $clientes->observations,
                'dateCreated' => date('Y-m-d'),
                'deleted' => false
            );
            $idCli = $asaas->Cliente()->create($dadosCliente);
            $cliente  = Clientes::find($clientes->id);
            $cliente->externalReference = $idCli->id;
            $cliente->save();
            echo $clientes->name." importado com sucesso. <br>";
            unset($dadosCliente);
        }
        $request->session()->flash('sucesso', 'Clientes importados do Asaas.');
        return redirect()->back();
    }

}
