<?php

namespace App\Http\Controllers;

use App\Models\Chips;
use Illuminate\Http\Request;

class ChipsController extends Controller{
    public function lista(){
        $title = "Chips";
        return view('chips.lista')->with(compact('title'));
    }

    public function add(){
        $title = "Adicionar Chip";
        return view('chips.add')->with(compact('title'));
    }

    public function todosChips(Request $request){
        $columns = array(
            0 =>'id',
            1 =>'uuid',
            2 =>'number',
            3 =>'carrier',
            4 =>'serviceProvider',
        );

        $totalData = Chips::count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if(empty($request->input('search.value'))){
            $chips = Chips::offset($start)->limit($limit)->orderBy($order,$dir)->get();
        }
        else{
            $search = $request->input('search.value');
            $chips =  Chips::where('uuid','LIKE',"%{$search}%")
                ->orwhere('number','LIKE',"%{$search}%")
                ->orwhere('carrier','LIKE',"%{$search}%")
                ->orwhere('serviceProvider','LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
            $totalFiltered = Chips::where('uuid','LIKE',"%{$search}%")
                ->orwhere('number','LIKE',"%{$search}%")
                ->orwhere('carrier','LIKE',"%{$search}%")
                ->orwhere('serviceProvider','LIKE',"%{$search}%")
                ->count();
        }
        $data = array();

        if(!empty($chips)){
            foreach ($chips as $chip){
                $nestedData['id'] = "# ".$chip->id;
                $nestedData['uuid'] =$chip->uuid;
                $nestedData['number'] =$chip->number;
                $nestedData['imei'] = "Ainda falta implementar";
                $nestedData['serviceProvider'] =$chip->serviceProvider;
                $nestedData['carrier'] =$chip->carrier;
                $nestedData['opcoes'] = "<a href=\"/Cliente/".$chip->id."\" class=\"btn btn-icon btn-success waves-effect waves-float waves-light\"><i data-feather='eye'></i></a>
                                            <a href=\"/EdtClientes/".$chip->id."\" class=\"btn btn-icon btn-warning waves-effect waves-float waves-light\"><i data-feather='edit'></i></i></a>
                                            <a href=\"/Cliente/".$chip->id."\" class=\"btn btn-icon btn-danger waves-effect waves-float waves-light\"><i data-feather='trash-2'></i></i></i></a>
                                            ";

                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);
    }


    public function teste(){
        $placa   = 'KCK2486';
        $token = hash_hmac('sha1', $placa, 'shienshenlhq', false);
        $request = '<?xml version="1.0" encoding="utf-8" standalone="yes" ?>'
            . '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" '
            . 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
            . 'xmlns:xsd="http://www.w3.org/2001/XMLSchema" >'
            . '<soap:Header>'
            . '<dispositivo>GT-S1312L</dispositivo>'
            . '<nomeSO>Android</nomeSO>'
            . '<versaoAplicativo>1.1.1</versaoAplicativo><versaoSO>4.1.4</versaoSO>'
            . '<aplicativo>aplicativo</aplicativo><ip>177.206.169.90</ip>'
            . '<token>'.$token.'</token>'
            . '<latitude>-3.6770324</latitude><longitude>-38.6791411</longitude></soap:Header><soap:Body><webs:getStatus xmlns:webs="http://soap.ws.placa.service.sinesp.serpro.gov.br/"><placa>'.$placa.'</placa></webs:getStatus></soap:Body></soap:Envelope>';


        $header = array(
            "Content-type: application/x-www-form-urlencoded; charset=UTF-8",
            "Accept: text/plain, */*; q=0.01",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "x-wap-profile: http://wap.samsungmobile.com/uaprof/GT-S7562.xml",
            "Content-length: ".strlen($request),
            "User-Agent: Mozilla/5.0 (Linux; U; Android 4.1.4; pt-br; GT-S1162L Build/IMM76I) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30",
        );

        $soap_do = curl_init();
        curl_setopt($soap_do, CURLOPT_URL, "http://sinespcidadao.sinesp.gov.br/sinesp-cidadao/ConsultaPlacaNovo27032014" );
        curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($soap_do, CURLOPT_TIMEOUT,        10);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($soap_do, CURLOPT_POST,           true );
        curl_setopt($soap_do, CURLOPT_POSTFIELDS, $request);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER,     $header);
        $res = curl_exec($soap_do);
        if($res === false)
        {
            $err = 'Curl erro: ' . curl_error($soap_do);
            curl_close($soap_do);
            print $err;
        }
        else
        {
            echo $res;
            curl_close($soap_do);
            print 'Ocorreu um erro...';
        }
    }
}
