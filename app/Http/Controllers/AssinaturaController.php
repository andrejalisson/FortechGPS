<?php

namespace App\Http\Controllers;

use App\Models\Assinaturas;
use App\Models\Clientes;
use Illuminate\Http\Request;

class AssinaturaController extends Controller{
    public function lista(){
        $title = "Assinaturas";
        return view('assinatura.lista')->with(compact('title'));
    }

    public function add(){
        $title = "Adicionar Assinaturas";
        $clientes = Clientes::all();
        return view('assinatura.add')->with(compact('title', 'clientes'));
    }

    public function todasAssinaturas(Request $request){
        $columns = array(
            0 =>'clienteId',
            1 =>'value',
            2 =>'description',
        );

        $totalData = Assinaturas::count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if(empty($request->input('search.value'))){
            $assinaturas = Assinaturas::offset($start)->limit($limit)->orderBy($order,$dir)->get();
        }
        else{
            $search = $request->input('search.value');
            $assinaturas =  Assinaturas::where('description','LIKE',"%{$search}%")
                ->orwhere('name','LIKE',"%{$search}%")
                ->orwhere('value','LIKE',"%{$search}%")
                ->leftJoin('clientes', 'clienteId', '=', 'clientes.id')
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
            $totalFiltered = Clientes::where('description','LIKE',"%{$search}%")
                ->orwhere('name','LIKE',"%{$search}%")
                ->orwhere('value','LIKE',"%{$search}%")
                ->count();
        }
        $data = array();
        if(!empty($assinaturas)){
            foreach ($assinaturas as $assinatura){
                $nestedData['nome'] =$assinatura->name;
                $nestedData['valor'] =$assinatura->value;
                $nestedData['descricao'] =$assinatura->description;
                $nestedData['metodo'] =$assinatura->billingType;
                $nestedData['vencimento'] =$assinatura->nextDueDate;
                $nestedData['opcoes'] = "<a href=\"/Cliente/".$assinatura->id."\" class=\"btn btn-icon btn-success waves-effect waves-float waves-light\"><i data-feather='eye'></i></a>
                                            <a href=\"/EdtClientes/".$assinatura->id."\" class=\"btn btn-icon btn-warning waves-effect waves-float waves-light\"><i data-feather='edit'></i></i></a>
                                            <a href=\"/Cliente/".$assinatura->id."\" class=\"btn btn-icon btn-danger waves-effect waves-float waves-light\"><i data-feather='trash-2'></i></i></i></a>
                                            ";

                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);
    }
}
