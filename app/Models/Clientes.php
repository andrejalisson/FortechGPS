<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clientes extends Model{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'clientes';
    protected $guarded = ['id'];
    protected $fillable = [
        'name',
        'cpfCnpj',
        'email',
        'phone',
        'mobilePhone',
        'address',
        'addressNumber',
        'complement',
        'province',
        'postalCode',
        'city',
        'state',
        'externalReference',
        'notificationDisabled',
        'municipalInscription',
        'stateInscription',
        'observations',
        'dateCreated',
        'deleted'];
}
