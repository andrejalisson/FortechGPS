<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chips extends Model{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'chips';
    protected $guarded = ['id'];
    protected $fillable = [
        'uuid',
        'serial',
        'number',
        'serviceProvider',
        'apn',
        'apnUsername',
        'apnPassword',
        'carrier'];
}
