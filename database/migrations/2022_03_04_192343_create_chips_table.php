<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chips', function (Blueprint $table) {
            $table->id();
            $table->string('uuid',20);
            $table->string('serial',20);
            $table->string('number',20);
            $table->string('serviceProvider',20);
            $table->string('apn',20);
            $table->string('apnUsername',20);
            $table->string('apnPassword',20);
            $table->string('carrier',20);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chips');
    }
}
