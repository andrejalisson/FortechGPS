<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('cpfCnpj', 14);
            $table->string('email', 50)->nullable();
            $table->string('phone', 10)->nullable();
            $table->string('mobilePhone', 11)->nullable();
            $table->string('address', 100)->nullable();
            $table->string('addressNumber', 10)->nullable();
            $table->string('complement', 30)->nullable();
            $table->string('province', 30)->nullable();
            $table->string('postalCode', 8)->nullable();
            $table->string('city', 10)->nullable();
            $table->string('state', 2)->nullable();
            $table->string('externalReference', 100)->nullable();
            $table->boolean('notificationDisabled')->nullable()->default(true);
            $table->string('municipalInscription', 20)->nullable();
            $table->string('stateInscription', 20)->nullable();
            $table->text('observations')->nullable();
            $table->date('dateCreated')->nullable();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
