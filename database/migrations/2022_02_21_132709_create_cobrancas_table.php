<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCobrancasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cobrancas', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('clienteId');
            $table->foreign('clienteId')->references('id')->on('clientes');
            $table->string('billingType', 11)->default("UNDEFINED");
            $table->decimal('value');
            $table->date('dueDate');
            $table->boolean('discount');
            $table->decimal('discountValue')->nullable();
            $table->integer('dueDateLimitDays')->nullable();
            $table->string('discountType', 10)->default("PERCENTAGE");
            $table->boolean('interest');
            $table->decimal('interestValue')->nullable();
            $table->boolean('fine');
            $table->decimal('fineValue')->nullable();
            $table->boolean('postalService');
            $table->text('description');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cobrancas');
    }
}
