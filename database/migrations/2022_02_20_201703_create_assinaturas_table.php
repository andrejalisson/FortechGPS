<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssinaturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assinaturas', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('clienteId');
            $table->foreign('clienteId')->references('id')->on('clientes');
            $table->string('billingType', 11)->default("UNDEFINED");
            $table->decimal('value');
            $table->date('nextDueDate');
            $table->boolean('discount');
            $table->decimal('discountValue')->nullable();
            $table->integer('dueDateLimitDays')->nullable();
            $table->string('discountType', 10)->default("PERCENTAGE");
            $table->boolean('interest');
            $table->decimal('interestValue')->nullable();
            $table->boolean('fine');
            $table->decimal('fineValue')->nullable();
            $table->string('cycle', 12)->default("MONTHLY");
            $table->text('description');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assinaturas');
    }
}
