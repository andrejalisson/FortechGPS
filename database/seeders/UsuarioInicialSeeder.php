<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsuarioInicialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('usuarios')->insert([
            'nome' => "André Jálisson Gonzaga de Sousa",
            'username' => "andrejalisson",
            'email' => "andrejalisson@gmail.com",
            'senha' => password_hash("31036700", PASSWORD_DEFAULT),
            'criacao' => date("Y-m-d H:i:s"),
            'tentativas' => 0,
            'status' => 1,
        ]);
        DB::table('usuarios')->insert([
            'nome' => "Marcelo Ferreira Bezerra",
            'username' => "marcelfb",
            'email' => "bezerra260@gmail.com",
            'senha' => password_hash("vi1712", PASSWORD_DEFAULT),
            'criacao' => date("Y-m-d H:i:s"),
            'tentativas' => 0,
            'status' => 1,
        ]);

    }
}
