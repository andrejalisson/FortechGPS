@extends('templates.admin')

@section('css')
    <link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/tables/datatable/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/tables/datatable/responsive.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/tables/datatable/buttons.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/tables/datatable/rowGroup.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css">
@endsection

@section('corpo')
    <!-- Basic table -->
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Chips</h2>
                </div>
            </div>
        </div>
        <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
            <div class="mb-1 breadcrumb-right">
                <a href="/AddChips" class="btn btn-outline-primary waves-effect">
                    <i data-feather='user-plus'></i>
                    <span>Adicionar Chip</span>
                </a>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <table id="clientes" class="datatables-basic table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>ICCID</th>
                            <th>Número</th>
                            <th>IMEI - Modelo</th>
                            <th>Operadora</th>
                            <th>Broker</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </section>
    </div>
    <!--/ Basic table -->
@endsection

@section('js')
    <script src="/app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js"></script>
    <script src="/app-assets/vendors/js/tables/datatable/dataTables.bootstrap5.min.js"></script>
    <script src="/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
    <script src="/app-assets/vendors/js/tables/datatable/responsive.bootstrap5.min.js"></script>
    <script src="/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
    <script src="/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js"></script>
@endsection

@section('script')
    <script>
        $(function () {
            $(document).ready(function () {
                $('#clientes').dataTable({
                    pageLength: 10,
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    oLanguage: {
                        "sLengthMenu": "Mostrar _MENU_ registros por página",
                        "sZeroRecords": "Nenhum registro encontrado",
                        "sInfo": "Mostrando _END_ de _TOTAL_ registro(s)",
                        "sInfoEmpty": "Mostrando 0 / 0 de 0 registros",
                        "sInfoFiltered": "(filtrado de _MAX_ registros)",
                        "sSearch": "Pesquisar: ",
                        "oPaginate": {
                            "sFirst": "Início",
                            "sPrevious": "Anterior",
                            "sNext": "Próximo",
                            "sLast": "Último"
                        }
                    },
                    ajax:{
                        "url": "{{ url('todosChips') }}",
                        "dataType": "json",
                        "type": "POST",
                        "data":{
                            _token: "{{csrf_token()}}"
                        }
                    },
                    columns: [
                        { "data": "id" },
                        { "data": "uuid" },
                        { "data": "number" },
                        { "data": "imei" },
                        { "data": "serviceProvider" },
                        { "data": "carrier" },
                        { "data": "opcoes" }
                    ]

                });


            });
        });

    </script>
@endsection
