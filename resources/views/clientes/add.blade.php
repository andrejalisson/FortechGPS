@extends('templates.admin')

@section('css')

@endsection

@section('corpo')
    <div class="content-body">
        <!-- Basic multiple Column Form section start -->
        <section id="multiple-column-form">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Cliente</h4>
                        </div>
                        <div class="card-body">
                            <form class="form" action="/AddClientes" method="POST">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label">CPF/CNPJ</label>
                                            <input type="text" autofocus class="form-control cpfOuCnpj" id="documento" required placeholder="000.000.000-00" name="documento" />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label">Nome/Nome Fantasia</label>
                                            <input type="text" id="nomeFantasia" class="form-control" required name="nome" placeholder="Nome/Nome Fantasia" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label">Email</label>
                                            <input type="email" id="email" class="form-control" name="email" placeholder="fulano@empresa.com.br" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label">Telefone</label>
                                            <input type="text" id="telefone" class="form-control" name="telefone" placeholder="(85)0000-0000" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label">Celular(Whatsapp)</label>
                                            <input type="text" id="celular" class="form-control" name="celular" placeholder="(85)0000-0000" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label">Inscrição Estadual</label>
                                            <input type="text" name="ie" id="ie" class="form-control" placeholder="00000000-0" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label">Inscrição Municipal</label>
                                            <input type="text"  name="im" class="form-control" placeholder="Inscrição Municipal" />
                                        </div>
                                    </div>
                                    <hr>
                                    <h4 class="card-title">Endereço</h4>
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label">CEP</label>
                                            <input type="text" id="cep" class="form-control" name="cep" placeholder="00000-000" />
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-12">
                                        <div class="mb-1">
                                            <label class="form-label">Logradouro</label>
                                            <input type="text" id="logradouro" class="form-control" name="logradouro" placeholder="Rua Alameida dos Tapebas" />
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-12">
                                        <div class="mb-1">
                                            <label class="form-label">Número</label>
                                            <input type="text" id="numero" class="form-control" name="numero" placeholder="1234" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="mb-1">
                                            <label class="form-label">Complemento</label>
                                            <input type="text" id="complemento" class="form-control" name="complemento" placeholder="Apartamento 123" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label">Bairo</label>
                                            <input type="text" id="bairro" class="form-control" name="bairro" placeholder="Parangaba" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label">Cidade</label>
                                            <input type="text" id="cidade" class="form-control" name="cidade" placeholder="Fortaleza" />
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-12">
                                        <div class="mb-1">
                                            <label class="form-label">Estado</label>
                                            <input type="text" id="uf" class="form-control" name="uf" placeholder="Ceará" />
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-12">
                                        <div class="mb-1">
                                            <label class="d-block form-label" for="validationBioBootstrap">Observações</label>
                                            <textarea class="form-control" id="validationBioBootstrap" name="obs" rows="3" required=""></textarea>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <button type="submit" class="btn btn-primary me-1">Salvar</button>
                                        <button type="reset" class="btn btn-outline-secondary" id="limpar">Limpar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Basic Floating Label Form section end -->

    </div>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
@endsection

@section('script')
    <script>
        $('#limpar').click(function(){
            $('#nomeFantasia').attr("readonly", false);
            $('#razaoSocial').attr("readonly", false);
            $('#abertura').attr("readonly", false);
            $("#documento").focus();
        })
        function limpa_formulário_cep() {
            // Limpa valores do formulário de cep.
            $("#logradouro").val("");
            $("#bairro").val("");
            $("#cidade").val("");
            $("#uf").val("");
        }
        var options = {
            onKeyPress: function (cpf, ev, el, op) {
                var masks = ['000.000.000-000', '00.000.000/0000-00'];
                $('.cpfOuCnpj').mask((cpf.length > 14) ? masks[1] : masks[0], op);
            }
        }
        $('.cpfOuCnpj').length > 11 ? $('.cpfOuCnpj').mask('00.000.000/0000-00', options) : $('.cpfOuCnpj').mask('000.000.000-00#', options);
        $("#telefone").mask("(00) 0000-0009");
        $("#celular").mask("(00) 0 0000-0009");
        $("#ie").mask("00000000-0");
        $("#cep").mask("00000-000");
        $('#documento').focusout( function(){
            var documento = $('#documento').val();
            if(documento.length <= 14){
                // $('#razaoSocial').attr("readonly", true);
            }else{
                // $('#razaoSocial').attr("readonly", false);
                documento = documento.replace('.','');
                documento = documento.replace('-','');
                documento = documento.replace('/','');
                documento = documento.replace('.','');
                $.ajax({
                    url: 'https://www.receitaws.com.br/v1/cnpj/'+documento,
                    dataType: 'jsonp',
                    type: 'GET',
                    success: function (data) {
                        console.log(data);
                        if(data.status == "OK"){
                            $('#nomeFantasia').val(data.fantasia);
                            $('#nomeFantasia').attr("readonly", true);
                            $('#email').val(data.email);
                            $('#email').attr("readonly", true);
                            $('#telefone').val(data.telefone);
                            $('#telefone').attr("readonly", true);
                            $('#cep').val(data.cep);
                            $('#cep').attr("readonly", true);
                            $('#logradouro').val(data.logradouro);
                            $('#logradouro').attr("readonly", true);
                            $('#numero').val(data.numero);
                            $('#numero').attr("readonly", true);
                            $('#complemento').val(data.complemento);
                            $('#complemento').attr("readonly", true);
                            $('#bairro').val(data.bairro);
                            $('#bairro').attr("readonly", true);
                            $('#cidade').val(data.municipio);
                            $('#cidade').attr("readonly", true);
                            $('#uf').val(data.uf);
                            $('#uf').attr("readonly", true);
                            $("#celular").focus();
                        }else{
                            $("#documento").focus();
                            $('#documento').val("");
                            iziToast.error({
                                title: 'Erro',
                                message: data.message
                            });
                        }

                    }
                });

            }
        });
        $("#cep").blur(function() {

            //Nova variável "cep" somente com dígitos.
            var cep = $(this).val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                    //Preenche os campos com "..." enquanto consulta webservice.
                    $("#logradouro").val("...");
                    $("#bairro").val("...");
                    $("#cidade").val("...");
                    $("#uf").val("...");

                    //Consulta o webservice viacep.com.br/
                    $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#logradouro").val(dados.logradouro);
                            $("#bairro").val(dados.bairro);
                            $("#cidade").val(dados.localidade);
                            $("#uf").val(dados.uf);
                            $("#numero").focus();
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            limpa_formulário_cep();
                            $("#cep").focus();
                            $('#cep').val("");
                            iziToast.error({
                                title: 'Erro',
                                message: "Cep não encontrado"
                            });
                        }
                    });
                } //end if.
                else {
                    //cep é inválido.
                    limpa_formulário_cep();
                    $("#cep").focus();
                    $('#cep').val("");
                    iziToast.error({
                        title: 'Erro',
                        message: "Cep não inválido"
                    });
                }
            } //end if.
            else {
                //cep sem valor, limpa formulário.
                limpa_formulário_cep();
            }
        });







    </script>

@endsection
