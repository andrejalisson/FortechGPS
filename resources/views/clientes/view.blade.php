@extends('templates.admin')

@section('css')
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/select/select2.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/animate/animate.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/extensions/sweetalert2.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/responsive.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/buttons.bootstrap5.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/rowGroup.bootstrap5.min.css">
@endsection

@section('corpo')
    @foreach($cliente as $cliente)
        @php
            $partes = explode(' ', $cliente->name);
            $primeiroNome = array_shift($partes);
            $ultimoNome = array_pop($partes);
            $partesPL = str_split($primeiroNome);
            $primeiraLetra = array_shift($partesPL);
            $partesUL = str_split($ultimoNome);
            $ultimaLetra = array_shift($partesUL);
        @endphp
        <div class="content-body">
            <section class="app-user-view-account">
                <div class="row">
                    <!-- User Sidebar -->
                    <div class="col-xl-4 col-lg-5 col-md-5 order-1 order-md-0">
                        <!-- User Card -->
                        <div class="card">
                            <div class="card-body">
                                <div class="user-avatar-section">
                                    <div class="d-flex align-items-center flex-column">

                                        <div class="avatar bg-light-success avatar-lg">
                                            <span class="avatar-content">{{$primeiraLetra."".$ultimaLetra}}</span>
                                        </div>
                                        <div class="user-info text-center">
                                            <h4>{{$cliente->name}}</h4>
                                            @if(strlen($cliente->cpfCnpj) == 11)
                                                <span class="badge bg-light-secondary">Pessoa Física</span>
                                                @php
                                                    $docFormatado = substr($cliente->cpfCnpj, 0, 3) . '.' .
                                                                   substr($cliente->cpfCnpj, 3, 3) . '.' .
                                                                   substr($cliente->cpfCnpj, 6, 3) . '.' .
                                                                   substr($cliente->cpfCnpj, 9, 2);
                                                @endphp
                                            @else
                                                <span class="badge bg-light-secondary">Pessoa Jurídica</span>
                                                @php
                                                    $docFormatado = substr($cliente->cpfCnpj, 0, 2) . '.' .
                                                                    substr($cliente->cpfCnpj, 2, 3) . '.' .
                                                                    substr($cliente->cpfCnpj, 5, 3) . '/' .
                                                                    substr($cliente->cpfCnpj, 8, 4) . '-' .
                                                                    substr($cliente->cpfCnpj, -2);
                                                @endphp
                                            @endif
                                        </div>
                                    </div>
                                </div>
{{--                                <div class="d-flex justify-content-around my-2 pt-75">--}}
{{--                                    <div class="d-flex align-items-start me-2">--}}
{{--                                        <span class="badge bg-light-primary p-75 rounded">--}}
{{--                                            <i data-feather="check" class="font-medium-2"></i>--}}
{{--                                        </span>--}}
{{--                                        <div class="ms-75">--}}
{{--                                            <h4 class="mb-0">345</h4>--}}
{{--                                            <small>Veiculos</small>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="d-flex align-items-start">--}}
{{--                                        <span class="badge bg-light-primary p-75 rounded">--}}
{{--                                            <i data-feather="briefcase" class="font-medium-2"></i>--}}
{{--                                        </span>--}}
{{--                                        <div class="ms-75">--}}
{{--                                            <h4 class="mb-0">568</h4>--}}
{{--                                            <small>Usuários</small>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <hr>
                                <h4 class="fw-bolder border-bottom pb-50 mb-1">Dados Pessoais</h4>
                                <div class="info-container">
                                    <ul class="list-unstyled">
                                        <li class="mb-75">
                                            <span class="fw-bolder me-25">CPF/CNPJ:</span>
                                            <span>{{$docFormatado}}</span>
                                        </li>
                                        <li class="mb-75">
                                            <span class="fw-bolder me-25">Nome/Nome Fantasia:</span>
                                            <span>{{$cliente->name}}</span>
                                        </li>
                                        <li class="mb-75">
                                            <span class="fw-bolder me-25">Status:</span>
                                            @if($cliente->deleted == 0)
                                                <span class="badge bg-light-success">Ativo</span>
                                            @else
                                                <span class="badge bg-light-danger">Inativo</span>
                                            @endif
                                        </li>
                                        <li class="mb-75">
                                            <span class="fw-bolder me-25">Email:</span>
                                            <span>{{$cliente->email}}</span>
                                        </li>
                                        <li class="mb-75">
                                            <span class="fw-bolder me-25">Telefone:</span>
                                            @php
                                                $telefone = "(".substr($cliente->phone, 0, 2) . ')' .
                                                                substr($cliente->phone, 2, 4) . '-' .
                                                                substr($cliente->phone, 5, 4);
                                            @endphp
                                            <span>{{$telefone}}</span>
                                        </li>
                                        <li class="mb-75">
                                            <span class="fw-bolder me-25">Celular:</span>
                                            @php
                                                $celular = "(".substr($cliente->mobilePhone, 0, 2) . ')' .
                                                                substr($cliente->mobilePhone, 2, 1) . ' ' .
                                                                substr($cliente->mobilePhone, 3, 4) . '-' .
                                                                substr($cliente->mobilePhone, 6, 4);
                                            @endphp
                                            <span>{{$celular}}</span>
                                        </li>
                                        <hr>
                                        <h4 class="fw-bolder border-bottom pb-50 mb-1">Endereço</h4>
                                        <li class="mb-75">
                                            <span class="fw-bolder me-25">CEP:</span>
                                            @php
                                                $cep = substr($cliente->postalCode, 0, 5)."-".
                                                                substr($cliente->postalCode, 5, 3);
                                            @endphp
                                            <span>{{$cep}}</span>
                                        </li>
                                        <li class="mb-75">
                                            <span class="fw-bolder me-25">Logradouro:</span>
                                            <span>{{$cliente->address.", ".$cliente->addressNumber}}</span>
                                        </li>
                                        <li class="mb-75">
                                            <span class="fw-bolder me-25">Complemento:</span>
                                            <span>{{$cliente->complement}}</span>
                                        </li>
                                        <li class="mb-75">
                                            <span class="fw-bolder me-25">Bairro:</span>
                                            <span>{{$cliente->province}}</span>
                                        </li>
                                        <li class="mb-75">
                                            <span class="fw-bolder me-25">Cidade/Estado:</span>
                                            <span>{{$cliente->city." - ".$cliente->state}}</span>
                                        </li>
                                    </ul>
                                    <div class="d-flex justify-content-center pt-2">
                                        <a href="/EdtClientes/{{$cliente->id}};" class="btn btn-primary me-1">Editar</a>
                                        <a href="javascript:;" class="btn btn-outline-danger suspend-user">Desativar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /User Card -->
                    </div>
                    <!--/ User Sidebar -->

                    <!-- User Content -->
                    <div class="col-xl-8 col-lg-7 col-md-7 order-0 order-md-1">
                        <!-- User Pills -->
                        <ul class="nav nav-pills mb-2">
                            <li class="nav-item">
                                <a class="nav-link active" href="/Cliente/{{$cliente->id}}">
                                    <i data-feather="user" class="font-medium-3 me-50"></i>
                                    <span class="fw-bold">Financeiro</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    <i data-feather="lock" class="font-medium-3 me-50"></i>
                                    <span class="fw-bold">Contratos</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    <i data-feather="bookmark" class="font-medium-3 me-50"></i>
                                    <span class="fw-bold">Notificações</span>
                                </a>
                            </li>
                        </ul>
                        <!--/ User Pills -->

                        <!-- Project table -->
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title mb-50">Cobranças</h4>
                                <button class="btn btn-primary btn-sm waves-effect waves-float waves-light">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg>
                                    <span>Add Cobrança Avulsa</span>
                                </button>
                            </div>
                            <div class="table-responsive">
                                <table class="table datatable-project" id="cobrancas">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text-nowrap">Valor</th>
                                        <th >Status</th>
                                        <th>Vencimento</th>
                                        <th>Opções</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <!-- /Project table -->
                        <!-- Project table -->
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title mb-50">Assinaturas</h4>
                                <a href="javascript:;" class="btn btn-primary btn-sm waves-effect waves-float waves-light" data-bs-target="#assinatura" data-bs-toggle="modal">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg>
                                    <span>Add Assinatura</span>
                                </a>
                            </div>
                            <div class="table-responsive">
                                <table class="table datatable-project" id="cobrancas">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text-nowrap">Valor</th>
                                        <th >Status</th>
                                        <th>Vencimento</th>
                                        <th>Opções</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title mb-50">Parceladas</h4>
                                <button class="btn btn-primary btn-sm waves-effect waves-float waves-light">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg>
                                    <span>Add Parcelamento</span>
                                </button>
                            </div>
                            <div class="table-responsive">
                                <table class="table datatable-project" id="cobrancas">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text-nowrap">Valor</th>
                                        <th >Status</th>
                                        <th >Parcela</th>
                                        <th>Vencimento</th>
                                        <th>Opções</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <!-- /Project table -->

                        <!-- Activity Timeline -->
                        <div class="card">
                            <h4 class="card-header">Ultimas Notificações</h4>
                            <div class="card-body pt-1">
                                <ul class="timeline ms-50">
                                    <li class="timeline-item">
                                        <span class="timeline-point timeline-point-indicator"></span>
                                        <div class="timeline-event">
                                            <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                                <h6>andrejalisson</h6>
                                                <span class="timeline-event-time me-1">12 min atrás</span>
                                            </div>
                                            <p>Logou às 12:12</p>
                                        </div>
                                    </li>
                                    <li class="timeline-item">
                                        <span class="timeline-point timeline-point-warning timeline-point-indicator"></span>
                                        <div class="timeline-event">
                                            <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                                <h6>andrejalisson</h6>
                                                <span class="timeline-event-time me-1">10 min atrás</span>
                                            </div>
                                            <p>cadastrou o motorista às 12:14</p>
                                            <div class="d-flex flex-row align-items-center mb-50">
                                                <div class="avatar me-50">
                                                    <img src="../../../app-assets/images/portrait/small/avatar-s-7.jpg" alt="Avatar" width="38" height="38" />
                                                </div>
                                                <div class="user-info">
                                                    <h6 class="mb-0">Ana Clara (motorista)</h6>
                                                    <p class="mb-0">{{uniqid("MT")}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="timeline-item">
                                        <span class="timeline-point timeline-point-info timeline-point-indicator"></span>
                                        <div class="timeline-event">
                                            <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                                <h6>andrejalisson</h6>
                                                <span class="timeline-event-time me-1">2 dias atrás</span>
                                            </div>
                                            <p>Adicionou um novo veículo(Carro VW - VolksWagen Parati 1.6 Mi Plus Total Flex 8V 4p HYD-8975)</p>
                                        </div>
                                    </li>
                                    <li class="timeline-item">
                                        <span class="timeline-point timeline-point-danger timeline-point-indicator"></span>
                                        <div class="timeline-event">
                                            <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                                <h6>andrejalisson</h6>
                                                <span class="timeline-event-time me-1">5 dias atrás</span>
                                            </div>
                                            <p class="mb-0">Criou uma nova fatura</p>
                                            <div class="d-flex flex-row align-items-center mt-50">
                                                <img class="me-1" src="../../../app-assets/images/icons/pdf.png" alt="data.json" height="25" />
                                                <h6 class="mb-0">FaturaClienteX.pdf</h6>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- /Activity Timeline -->
                    </div>
                    <!--/ User Content -->
                </div>
            </section>
        </div>
        <!-- Edit User Modal -->
        <div class="modal fade" id="assinatura" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-edit-user">
                <div class="modal-content">
                    <div class="modal-header bg-transparent">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body pb-5 px-sm-5 pt-50">
                        <div class="text-center mb-2">
                            <h1 class="mb-1">Adicionar Assinatura.</h1>
                            <p>Dados da assinatura</p>
                        </div>
                        <form id="editUserForm" class="row gy-1 pt-75" onsubmit="return false">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <label class="form-label" for="modalEditUserStatus">Forma de pagamento</label>
                                    <select class="select2 form-select" id="selectForma">
                                        <option selected>Selecione a forma de pagamento</option>
                                        <option value="BOLETO">Boleto</option>
                                        <option value="CREDIT_CARD">Cartão de Crédito</option>
                                        <option value="PIX">PIX</option>
                                        <option value="UNDEFINED">Pergunte ao Cliente</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="form-label" for="modalEditUserStatus">Ciclo de Pagamento</label>
                                <select class="select2 form-select select2-basic" id="selectCiclo">
                                    <option selected>Selecione o ciclo de pagamento</option>
                                    <option value="WEEKLY">Semanal</option>
                                    <option value="BIWEEKLY">Quinzenal</option>
                                    <option value="MONTHLY">Mensal</option>
                                    <option value="QUARTERLY">Trimestral</option>
                                    <option value="SEMIANNUALLY">Semestral</option>
                                    <option value="YEARLY">Anual</option>
                                </select>
                            </div>
                            <div class="col-12 col-md-2">
                                <label class="form-label" for="modalEditUserFirstName">Valor fixo</label>
                                <input type="text" id="modalEditUserFirstName" name="modalEditUserFirstName" class="form-control" placeholder="John" value="Gertrude" data-msg="Please enter your first name" />
                            </div>
                            <div class="col-12 col-md-4">
                                <label class="form-label" for="modalEditUserLastName">Vencimento da primeira parcela</label>
                                <input type="text" id="modalEditUserLastName" name="modalEditUserLastName" class="form-control" placeholder="Doe" value="Barton" data-msg="Please enter your last name" />
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="form-label" for="modalEditUserEmail">Juros<small>(porcentagem)</small> </label>
                                <input type="text" id="modalEditUserEmail" name="modalEditUserEmail" class="form-control" value="gertrude@gmail.com" placeholder="example@domain.com" />
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="form-label" for="modalEditUserEmail">Multa<small>(porcentagem)</small></label>
                                <input type="text" id="modalEditUserEmail" name="modalEditUserEmail" class="form-control" value="gertrude@gmail.com" placeholder="example@domain.com" />
                            </div>

                            <div class="col-12">
                                <div class="d-flex align-items-center mt-1">
                                    <div class="form-check form-switch form-check-primary">
                                        <input type="checkbox" class="form-check-input" id="customSwitch10" checked />
                                        <label class="form-check-label" for="customSwitch10">
                                            <span class="switch-icon-left"><i data-feather="check"></i></span>
                                            <span class="switch-icon-right"><i data-feather="x"></i></span>
                                        </label>
                                    </div>
                                    <label class="form-check-label fw-bolder" for="customSwitch10">Use as a billing address?</label>
                                </div>
                            </div>
                            <div class="col-12 text-center mt-2 pt-50">
                                <button type="submit" class="btn btn-primary me-1">Submit</button>
                                <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                    Discard
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--/ Edit User Modal -->
    @endforeach
@endsection

@section('js')
    <script src="/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="/app-assets/vendors/js/forms/cleave/cleave.min.js"></script>
    <script src="/app-assets/vendors/js/forms/cleave/addons/cleave-phone.us.js"></script>
    <script src="/app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <script src="/app-assets/vendors/js/extensions/moment.min.js"></script>
    <script src="/app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js"></script>
    <script src="/app-assets/vendors/js/tables/datatable/dataTables.bootstrap5.min.js"></script>
    <script src="/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
    <script src="/app-assets/vendors/js/tables/datatable/responsive.bootstrap5.js"></script>
    <script src="/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
    <script src="/app-assets/vendors/js/tables/datatable/jszip.min.js"></script>
    <script src="/app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
    <script src="/app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
    <script src="/app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
    <script src="/app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
    <script src="/app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js"></script>
    <script src="/app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <script src="/app-assets/vendors/js/extensions/polyfill.min.js"></script>


@endsection

@section('script')
    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>

@endsection
