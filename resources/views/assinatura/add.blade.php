@extends('templates.admin')

@section('css')
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/select/select2.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css">
@endsection

@section('corpo')
    <div class="content-body">
        <!-- Basic multiple Column Form section start -->
        <section id="multiple-column-form">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Assinatura</h4>
                        </div>
                        <div class="card-body">
                            <form class="form" action="/AddClientes" method="POST">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class=mb-1">
                                            <label class="form-label" for="select2-basic">Cliente</label>
                                            <select class="select2 form-select" name="cliente">
                                                @foreach($clientes as $cliente)
                                                    <option value="{{$cliente->id}}">{{$cliente->name." - ".$cliente->cpfCnpj}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class=mb-1">
                                            <label class="form-label" for="select2-basic">Forma de Pagamento</label>
                                            <select class="select3 form-select" name="metodo">
                                                <option value="BOLETO">Boleto</option>
                                                <option value="CREDIT_CARD">Cartão de Crédito</option>
                                                <option value="PIX">Pix</option>
                                                <option selected value="UNDEFINED">Perguntar ao Cliente</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label">Valor</label>
                                            <input type="text" class="form-control" id="valor" name="valor" placeholder="R$ 0,00" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class=mb-1">
                                            <label class="form-label" for="select2-basic">Ciclo</label>
                                            <select class="select3 form-select" name="ciclo">
                                                <option value="WEEKLY">Semanal</option>
                                                <option value="BIWEEKLY">Quinzenal</option>
                                                <option selected value="MONTHLY">Mensal</option>
                                                <option value="QUARTERLY">Trimestral</option>
                                                <option value="SEMIANNUALLY">Semestral</option>
                                                <option value="YEARLY">Anual</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="fp-default">Vencimento da primeira cobrança</label>
                                            <input type="text" id="fp-default" name="vencimento" class="form-control flatpickr-basic" placeholder="{{date('d/m/Y')}}" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label">Juros</label>
                                            <input type="number" id="juros" class="form-control" name="juros" placeholder="0,00" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="mb-1">
                                            <label class="form-label">Multa</label>
                                            <input type="number" id="multa" class="form-control" name="multa" placeholder="0,00" />
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-12">
                                        <div class="mb-1">
                                            <label class="d-block form-label" for="validationBioBootstrap">Observações</label>
                                            <textarea class="form-control" id="validationBioBootstrap" name="obs" rows="3" maxlength="500" required=""></textarea>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <button type="submit" class="btn btn-primary me-1">Salvar</button>
                                        <button type="reset" class="btn btn-outline-secondary" id="limpar">Limpar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Basic Floating Label Form section end -->

    </div>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
    <script src="/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js"></script>

@endsection

@section('script')
    <script>
        (function (window, document, $) {
            'use strict';
            var select = $('.select2');
            var select2 = $('.select3');
            // var basicPickr = $('.flatpickr-basic');

            select.each(function () {
                var $this = $(this);
                $this.wrap('<div class="position-relative"></div>');
                $this.select2({
                    // the following code is used to disable x-scrollbar when click in select input and
                    // take 100% width in responsive also
                    dropdownAutoWidth: true,
                    width: '100%',
                    dropdownParent: $this.parent()
                });
            });
            select2.each(function () {
                var $this = $(this);
                $this.wrap('<div class="position-relative"></div>');
                $this.select2({
                    // the following code is used to disable x-scrollbar when click in select input and
                    // take 100% width in responsive also
                    dropdownAutoWidth: true,
                    width: '100%',
                    dropdownParent: $this.parent()
                });
            });
            $(".flatpickr-basic").flatpickr({
                dateFormat: "d/m/Y"
            });
            $("#juros").mask("0.00");
            $("#multa").mask("0.00");
            $("#valor").mask("0.00");
        })(window, document, jQuery);
    </script>

@endsection
