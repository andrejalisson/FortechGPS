<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\AcessoController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\AssinaturaController;
use App\Http\Controllers\ChipsController;

Route::get('/', [SiteController::class, 'home']);
Route::get('/Sobre', [SiteController::class, 'sobre']);
Route::get('/Planos', [SiteController::class, 'planos']);
Route::get('/Assistencia', [SiteController::class, 'assistencia']);

Route::get('/Login', [AcessoController::class, 'login']);
Route::get('/EsqueceuSenha', [AcessoController::class, 'forgot']);
Route::post('/Verifica', [AcessoController::class, 'verifica']);
Route::get('/Recuperar/{token}', [AcessoController::class, 'verificaToken']);
Route::post('/Recuperar', [AcessoController::class, 'novasenha']);
Route::get('/Sair', [AcessoController::class, 'logout']);

Route::get('/Clientes', [ClienteController::class, 'lista']);
Route::post('/todosClientes', [ClienteController::class, 'todosClientes'])->name('todosClientes');
Route::get('/AddClientes', [ClienteController::class, 'add']);
Route::post('/AddClientes', [ClienteController::class, 'addPost']);
Route::get('/EdtClientes/{id}', [ClienteController::class, 'edt']);
Route::post('/EdtClientes/{id}', [ClienteController::class, 'edtPost']);
Route::get('/Cliente/{id}', [ClienteController::class, 'cliente']);
Route::get('/ImportarAsaas', [ClienteController::class, 'importAsaas']);
Route::get('/ImportarAsaasHomolog', [ClienteController::class, 'importAsaasHomolog']);

Route::get('/Assinaturas', [AssinaturaController::class, 'lista']);
Route::get('/AddAssinatura', [AssinaturaController::class, 'add']);
Route::get('/AddAssinatura/{$id}', [AssinaturaController::class, 'addId']);
Route::post('/todasAssinaturas', [AssinaturaController::class, 'todasAssinaturas'])->name('todasAssinaturas');

Route::get('/Chips', [ChipsController::class, 'lista']);
Route::get('/AddChips', [ChipsController::class, 'add']);
Route::get('/Teste', [ChipsController::class, 'teste']);
Route::get('/AddChips/{$id}', [ChipsController::class, 'addId']);
Route::post('/todosChips', [ChipsController::class, 'todosChips'])->name('todosChips');

